import paho.mqtt.client as mqtt
import time
import json
#import csv
import datetime
from csv import writer

# init
'''
Male = 0
Total = 0
Female = 0
FlowRate = 0
id = 0
Power1 = 0
Power2 = 0

x = datetime.datetime.now()
month = (str(x.month)).zfill(2)
day = (str(x.day)).zfill(2)
year = str(x.year)
#print(year+month+day)
filename = 'log'+year+month+day+'.csv'
#print(filename)
init_csvFile()
'''

def init_csvFile():
    with open(filename, 'a', newline='') as f_object:
        list_data = ['id', 'Male', 'Female', 'Total', 'FlowRate','Power1' ,'Power2', 'Time']
        writer_object = writer(f_object)
        writer_object.writerow(list_data)
        f_object.close()

def on_disconnect(client, userdata, message):
    print("Disonnected from broker")
    global bConnected
    bConnected = False

def on_connect(client, userdata, flags, message):
    print("Connected to broker")
    global bConnected
    bConnected = True
    client.loop_start()
    client.subscribe("ATAL/LoRaApp/Client/17/Location/#")
#    client.subscribe("#")
    client.on_message = on_message


def on_message(client, userdata, message):
    global Male, Total, Female, FlowRate, id, Power1, Power2
    update = False

    rec = json.loads(message.payload.decode("utf-8"))
#    print(rec)
    try:

        InternalData = rec['InternalData']
        print(InternalData)
        Data = rec['Data']

        if InternalData['LocationID'] == 896:
            Male = Data['PeopleCumulativeDaily']
            update = True
        elif InternalData['LocationID'] == 898:
            Total = Data['PeopleCumulativeDaily']
            update = True
        elif InternalData['LocationID'] == 897:
            Female = Data['PeopleCumulativeDaily']
            update = True
        elif InternalData['LocationID'] == 910:
            FlowRate = Data['FlowRate']
            update = True
        elif InternalData['LocationID'] == 902:
            Power1 = Data['P']
            update = True
        elif InternalData['LocationID'] == 903:
            Power2 = Data['P']
            update = True
    except:
        print("Topic goes wrong!")

    if update == True:
        list_data = [id, Male, Female, Total, FlowRate, Power1, Power2, x.strftime("%X")]
        id += 1
        with open(filename, 'a', newline='') as f_object:
        # Pass the CSV  file object to the writer() function
            writer_object = writer(f_object)
        # Result - a writer object
        # Pass the data in the list as an argument into the writerow() function
            writer_object.writerow(list_data)
        # Close the file object
            f_object.close()
#    writer.writerow([4, "HELLO", datetime.datetime.now()])

#
# init
Male = 0
Total = 0
Female = 0
FlowRate = 0
id = 0
Power1 = 0
Power2 = 0
bConnected = False

x = datetime.datetime.now()
month = (str(x.month)).zfill(2)
day = (str(x.day)).zfill(2)
year = str(x.year)
#print(year+month+day)
filename = 'log'+year+month+day+'.csv'
#print(filename)
init_csvFile()


mqttBroker = "47.52.112.32"
port = 31883
client = mqtt.Client("home")
client.on_disconnect = on_disconnect
client.on_connect = on_connect
'''
client.connect(mqttBroker, port)
client.loop_start()
client.subscribe("ATAL/LoRaApp/Client/17/Location/#")
client.on_message = on_message
'''

while True:
   time.sleep(10)
   while not bConnected:
       try:
           print("Trying to connect broker")
           client.connect(mqttBroker, port)
           client.loop_start()
           #           client.loop_forever()
           time.sleep(5)

       except:
           bConnected = False


   pre_filename = filename
   x = datetime.datetime.now()
   month = (str(x.month)).zfill(2)
   day = (str(x.day)).zfill(2)
   year = str(x.year)
#   print(year+month+day)
   filename = 'log' + year + month + day + '.csv'
   if filename != pre_filename:
       id = 0
       init_csvFile()

'''
       with open(filename, 'a', newline='') as f_object:
           list_data = ['id', 'Male', 'Female', 'Total', 'FlowRate','Power1','Power2', 'Time']
           writer_object = writer(f_object)
           writer_object.writerow(list_data)
           f_object.close()
'''
#client.loop_end()








