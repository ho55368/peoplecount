FROM python:3.9

WORKDIR /usr/src/app

COPY requirements.txt .

COPY main.py .

#RUN pip install –upgrade setuptools

#RUN python3 -m pip3 install -U pip

#RUN pip install ez_setup

#RUN pip install --upgrade setuptools
RUN pip install setuptools~=57.5.0

RUN python -m pip install --upgrade pip

RUN pip install --no-cache-dir -r requirements.txt

# Expose HTTP port 80
EXPOSE 80
# Expose LoraServer API port 8080
EXPOSE 8080
# Expose MQTT port 1883
EXPOSE 31883
# Expose MySQL port 3306


CMD ["python", "./main.py"]